#!/usr/bin/env python3
# coding=utf-8

import math
from imports.constants import *

class Planet:
    AU = 1.495978707e11       # Astronomical Unit
    G  = 6.6743015151515e-11  # Gravitational Constant
    if WHOLE_SYSTEM is True:  # Whole System
        SCALE = 15 / AU       #     Default value: 15
    else:                     # Inner Planets only
        SCALE = 320 / AU      #     Default value: 320
    
    TIMESTEP = 3600*24*SPEED  # 1 day times SPEED

    def __init__(self, name, icon, color, mass, radius, x, y):
        self.name            = name
        self.icon            = icon
        self.color           = color
        self.mass            = mass
        self.radius          = radius
        self.x               = x
        self.y               = y

        self.orbit           = []
        self.sun             = False
        self.distance_to_sun = 0

        self.x_vel           = 0
        self.y_vel           = 0

        self.max_length      = 0


    def draw(self, win):
        x = self.x * self.SCALE + SCREEN_WIDTH / 2
        y = self.y * self.SCALE + SCREEN_HEIGHT / 2

        if len(self.orbit) > 2:
            updated_points = []
            for point in self.orbit:
                x, y = point
                x = x * self.SCALE + SCREEN_WIDTH / 2
                y = y * self.SCALE + SCREEN_HEIGHT / 2
                updated_points.insert(0, (x, y))    # Keep position table at minimum size

            if SHOW_ORBITS is True:
                if WHOLE_SYSTEM is False:
                    pygame.draw.lines(win, self.color, False, updated_points, ORBIT_LINE_WIDTH)

        win.blit(self.icon, (x - self.radius,y - self.radius))
		
        if not self.sun:
            if SHOW_TEXT is True:
                title       = TITLE_FONT.render(f"SOLAR SYSTEM SIMULATOR", 1, COLOR_TITLE)
                inner_title = TITLE_FONT.render(f"INNER PLANETS", 1, COLOR_TITLE)
                underscore  = TITLE_FONT.render(f"-------------------------------------", 1, COLOR_TITLE)
                win.blit(title, (65, 50))
                win.blit(underscore, (65, 70))
                if WHOLE_SYSTEM is False:
                    win.blit(inner_title, (120, 90))

                
    def attraction(self, other):                    # Calculate gravitational forces
        other_x, other_y = other.x, other.y
        distance_x = other_x - self.x
        distance_y = other_y - self.y
        distance = math.sqrt(distance_x ** 2 + distance_y ** 2)

        if other.sun:
            self.distance_to_sun = distance

        force = self.G * self.mass * other.mass / distance**2
        theta = math.atan2(distance_y, distance_x)
        force_x = math.cos(theta) * force
        force_y = math.sin(theta) * force
        return force_x, force_y

    def update_position(self, planets):             # Update planetary positions
        total_fx = total_fy = 0
        for planet in planets:
            if self is planet:
                continue

            fx, fy = self.attraction(planet)
            total_fx += fx
            total_fy += fy

        self.x_vel += total_fx / self.mass * self.TIMESTEP
        self.y_vel += total_fy / self.mass * self.TIMESTEP

        self.x += self.x_vel * self.TIMESTEP
        self.y += self.y_vel * self.TIMESTEP
        
        self.orbit.append((self.x, self.y))
        if WHOLE_SYSTEM is True:
            if len(self.orbit) > 300:
                self.orbit = self.orbit[1:]         # Limit orbit trail size (All System)
        else:
            if len(self.orbit) > 100:
                self.orbit = self.orbit[1:]         # Limit orbit trail size (Inner Planets only)


def main():
    run = True
    clock = pygame.time.Clock()
    
    sun           = Planet("Sun",                   # Name
                           sun_image,               # Icon
                           COLOR_SUN,               # Color
                           1.989e30,                # Mass
                           sun_radius,              # Radius
                           0,                       # Initial x position
                           0)                       # Initial y position
    sun.sun       = True

    mercury       = Planet("Mercury",               # Name
                           mercury_image,           # Icon
                           COLOR_MERCURY,           # Color
                           3.3010e23,               # Mass
                           mercury_radius,          # Radius
                           5.7909227e10,            # Initial x position
                           0)                       # Initial y position
    mercury.y_vel = -4.7362e4                       # Initial orbital velocity (m/s)

    venus         = Planet("Venus",                 # Name
                           venus_image,             # Icon
                           COLOR_VENUS,             # Color
                           4.8673e24,               # Mass
                           venus_radius,            # Radius
                           -1.0820948e11,           # Initial x position
                           0)                       # Initial y position
    venus.y_vel   = 3.5020e4                        # Initial orbital velocity (m/s)

    earth         = Planet("Earth",                 # Name
                           earth_image,             # Icon
                           COLOR_EARTH,             # Color
                           5.9722e24,               # Mass (kg)
                           earth_radius,            # Radius (km)
                           1.4959826e11,            # Initial x position
                           0)                       # Initial y position
    earth.y_vel   = -2.9783e4                       # Initial orbital velocity (m/s)

    moon          = Planet("Moon",                  # Name
                           moon_image,              # Icon
                           COLOR_MOON,              # Color
                           7.3477e22,               # Mass (kg)
                           moon_radius,             # Radius (km)
                           1.4959826e11 + 3.844e8,  # Initial x position
                           0)                       # Initial y position
    moon.y_vel    = -2.9783e4-1.022e3               # Initial orbital velocity (m/s)

    mars          = Planet("Mars",                  # Name
                           mars_image,              # Icon
                           COLOR_MARS,              # Color
                           6.4169e23,               # Mass
                           mars_radius,             # Radius
                           -2.2794382e11,           # Initial x position
                           0)                       # Initial y position
    mars.y_vel    = 2.4077e4                        # Initial orbital velocity (m/s)

    jupiter       = Planet("Jupiter",               # Name
                           jupiter_image,           # Icon
                           COLOR_JUPITER,           # Color
                           1.8981e27,               # Mass
                           jupiter_radius,          # Radius
                           7.7834082e11,            # Initial x position
                           0)                       # Initial y position
    jupiter.y_vel = -1.3056e4                       # Initial orbital velocity (m/s)

    saturn        = Planet("Saturn",                # Name
                           saturn_image,            # Icon
                           COLOR_SATURN,            # Color
                           5.6832e26,               # Mass
                           saturn_radius,           # Radius
                           -1.4266664e12,           # Initial x position
                           0)                       # Initial y position
    saturn.y_vel  = 9.6391e3                        # Initial orbital velocity (m/s)

    uranus        = Planet("Uranus",                # Name
                           uranus_image,            # Icon
                           COLOR_URANUS,            # Color
                           8.6810e25,               # Mass
                           uranus_radius,           # Radius
                           2.8706582e12,            # Initial x position
                           0)                       # Initial y position
    uranus.y_vel  = -6.7991e3                       # Initial orbital velocity (m/s)

    neptune       = Planet("Neptune",               # Name
                           neptune_image,           # Icon
                           COLOR_NEPTUNE,           # Color
                           1.0241e26,               # Mass
                           neptune_radius,          # Radius
                           -4.4983964e12,           # Initial x position
                           0)                       # Initial y position
    neptune.y_vel = 5.4349e3                        # Initial orbital velocity (m/s)

    pluto         = Planet("Pluto",                 # Name
                           pluto_image,             # Icon
                           COLOR_PLUTO,             # Color
                           1.3090e22,               # Mass
                           pluto_radius,            # Radius
                           5.9064406e12,            # Initial x position
                           0)                       # Initial y position
    pluto.y_vel   = -4.6691e3                       # Initial orbital velocity (m/s)

    ceres         = Planet("Ceres",                 # Name
                           ceres_image,             # Icon
                           COLOR_CERES,             # Color
                           9.4700e20,               # Mass
                           ceres_radius,            # Radius
                           -4.1369025e11,           # Initial x position
                           0)                       # Initial y position
    ceres.y_vel   = 1.7878e4                        # Initial orbital velocity (m/s)     

    # List of all planets
    planets = [sun, mercury, venus, earth, mars, jupiter, saturn, uranus, neptune, pluto, ceres]

    while run:
        clock.tick(60)
        CANVAS.blit(background, (0, 0))      # Draw background
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False                  # Quit simulation

        for planet in planets[1:]:           # Prevent Sun from moving
            planet.update_position(planets)  # Update planet positions

        for planet in planets:
            planet.draw(CANVAS)              # Draw Sun planets on screen

        pygame.display.update()              # Refresh screen

    pygame.quit()                            # Quit simulation


if __name__ == '__main__':
    main()