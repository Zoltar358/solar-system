import pygame

pygame.init()

background = pygame.image.load("resources/stars (2560x1440).jpg")  # Background

SCREEN_WIDTH, SCREEN_HEIGHT =  2560, 1440
CANVAS = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT), pygame.FULLSCREEN)
pygame.display.set_caption("Solar System Simulation")

# -----------------------------------------------------------------------------
WHOLE_SYSTEM     = False     # Default value: False
SHOW_ORBITS      = True      # Default value: True
ORBIT_LINE_WIDTH = 2         #     Width of orbit lines
SHOW_TEXT        = True      # Project title. Default value: True
# -----------------------------------------------------------------------------

if WHOLE_SYSTEM is True:     # Whole System
    SPEED       =   2        #    Default value: 8
    EARTH_SIZE  =   8        #    Default value: 5
else:                        # Inner Planets only
    SPEED       =   0.5      #    Default value: 0.5
    EARTH_SIZE  =  80        #    Default value: 80


# Radiuses (for display purpose only)
    # Sun radius (displayed as slightly larger than Earth)
sun_radius     = round(EARTH_SIZE *  1.2 / 2, 0)  #  1.2 times larger than Earth

    # Planet radiuses, relatve to Earth
mercury_radius = round(EARTH_SIZE /  2.6 / 2, 0)  #  2.6 times smaller than Earth
venus_radius   = round(EARTH_SIZE /  1.1 / 2, 0)  #  1.1 times smaller than Earth
earth_radius   = round(EARTH_SIZE *  1.0 / 2, 0)  #  1.0 times (Earth's size)
moon_radius    = round(EARTH_SIZE /  3.7 / 2, 0)  #  3.7 times smaller than Earth
mars_radius    = round(EARTH_SIZE /  1.9 / 2, 0)  #  1.9 times smaller than Earth
jupiter_radius = round(EARTH_SIZE * 11.0 / 2, 0)  # 11.0 times larger than Earth
saturn_radius  = round(EARTH_SIZE *  9.1 / 2, 0)  #  9.1 times larger than Earth
uranus_radius  = round(EARTH_SIZE *  4.0 / 2, 0)  #  4.0 times larger than Earth
neptune_radius = round(EARTH_SIZE *  3.9 / 2, 0)  #  3.9 times larger than Earth
pluto_radius   = round(EARTH_SIZE /  5.5 / 2, 0)  #  5.5 times smaller than Earth
ceres_radius   = round(EARTH_SIZE / 13.4 / 2, 0)  # 13.4 times smaller than Earth

# Sun icon (scaled to look nice on screen)
sun_image     = pygame.image.load('resources/sun-icon.png')
sun_image     = pygame.transform.scale(sun_image,(sun_radius * 2, sun_radius * 2))

# Planet icons (scaled to look nice on screen)
mercury_image = pygame.image.load('resources/mercury-icon.png')
mercury_image = pygame.transform.scale(mercury_image,(mercury_radius * 2, mercury_radius * 2))
venus_image   = pygame.image.load('resources/venus-icon.png')
venus_image   = pygame.transform.scale(venus_image, (venus_radius * 2, venus_radius * 2))
earth_image   = pygame.image.load('resources/earth-icon.png')
earth_image   = pygame.transform.scale(earth_image, (earth_radius * 2, earth_radius * 2))
moon_image    = pygame.image.load('resources/moon-icon.png')
moon_image    = pygame.transform.scale(moon_image, (moon_radius * 2, moon_radius * 2))
mars_image    = pygame.image.load('resources/mars-icon.png')
mars_image    = pygame.transform.scale(mars_image, (mars_radius * 2, mars_radius * 2))
jupiter_image = pygame.image.load('resources/jupiter-icon.png')
jupiter_image = pygame.transform.scale(jupiter_image, (jupiter_radius * 2, jupiter_radius * 2))
saturn_image  = pygame.image.load('resources/saturn-icon.png')
saturn_image  = pygame.transform.scale(saturn_image, (saturn_radius * 2, saturn_radius * 2))
uranus_image  = pygame.image.load('resources/uranus-icon.png')
uranus_image  = pygame.transform.scale(uranus_image, (uranus_radius * 2, uranus_radius * 2))
neptune_image = pygame.image.load('resources/neptune-icon.png')
neptune_image = pygame.transform.scale(neptune_image, (neptune_radius * 2, neptune_radius * 2))
pluto_image   = pygame.image.load('resources/pluto-icon.png')
pluto_image   = pygame.transform.scale(pluto_image, (pluto_radius * 2, pluto_radius * 2))
ceres_image   = pygame.image.load('resources/ceres-icon.png')
ceres_image   = pygame.transform.scale(ceres_image, (ceres_radius * 2, ceres_radius * 2))

# Colors for orbit lines
COLOR_SUN     = (255, 255,  20)  # Sun
COLOR_MERCURY = (80,   78,  81)  # Mercury
COLOR_VENUS   = (185, 185,  92)  # Venus
COLOR_EARTH   = (  0,  85, 255)  # Earth
COLOR_MOON    = (156, 156, 156)  # Moon
COLOR_MARS    = (167,  56,   0)  # Mars
COLOR_JUPITER = (175, 124,   3)  # Jupiter
COLOR_SATURN  = (250, 176,   5)  # Saturn
COLOR_URANUS  = (140, 250, 246)  # Uranus
COLOR_NEPTUNE = ( 23, 151, 250)  # Neptune
COLOR_PLUTO   = (199, 118,   5)  # Pluto
COLOR_CERES   = (255,   0, 255)  # Ceres
COLOR_TEXT    = (  0, 255,  30)  # Text info
COLOR_TITLE   = (255, 170,   0)  # Title

TITLE_FONT    = pygame.font.SysFont("Comic Sans MS", 20)  # Simulation title font
